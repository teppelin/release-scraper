FROM openjdk:latest

COPY build/libs/releases-1.0-SNAPSHOT-all.jar /usr/src/app/
WORKDIR /usr/src/app

CMD ["java", "-Xmx2g", "-jar", "releases-1.0-SNAPSHOT-all.jar"]
