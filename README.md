# releases

various media releases scraper (amqp worker)

## Usage

Requires the following environment variable set:

| variable      |  description                                            |
|---------------|---------------------------------------------------------|
| AMQP_URI      | [RabbitMQ URI](https://www.rabbitmq.com/uri-spec.html)  |
| PARALLELISM   | The number of jobs to process in corotuines in parallel |
| QUEUE_CONSUME | The name of the queue to consume jobs from              |
| QUEUE_PUBLISH | The name of the queue to publish to                     |

### Running

Download compile the JAR and run..
```sh
export VAR=.. # or inline them

java -jar releases.jar
# or use the script gradle generates..
./releases 
```
### Building a shadow Jar

```bash
./gradlew shadowJar
```
