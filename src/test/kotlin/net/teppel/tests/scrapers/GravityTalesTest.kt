package net.teppel.tests.scrapers

import io.kotlintest.matchers.types.shouldBeInstanceOf
import io.kotlintest.specs.StringSpec
import net.teppel.scrapers.GravityTales

private val logger = mu.KotlinLogging.logger {}

class GravityTalesTest : StringSpec() {
    init {
        val gravityTales = GravityTales()

        "should get novel list" {
            val list = gravityTales.list()
            list.shouldBeInstanceOf<List<GravityTales.MenuNovel>>()
        }

        "should get a novel" {

        }
    }
}
