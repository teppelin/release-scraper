package net.teppel.helpers

import com.github.kittinunf.fuel.core.extensions.jsonBody
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.serialization.responseObject
import com.github.kittinunf.result.Result
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

@Serializable
data class SearchResult(
    var total: Int,
    var items: List<SearchItem>
)

@Serializable
data class SearchItem(
    var id: Int,
    var name: String,
    var slug: String,
    var coverUrl: String,
    var abbreviation: String,
    var synopsis: String,
    var language: String,
    var timeCreated: String,
    var status: Int,
    var chapterCount: Int,
    var tags: List<String>
)

fun novelsList(source: String): SearchResult? {
    // The search endpoint doesn't expose a "translator" property
    // however the /novels one does but it's missing some novels..
    val (_, _, res) = @UseExperimental(kotlinx.serialization.ImplicitReflectionSerializer::class)
    "https://www.$source.com/api/novels/search"
        .httpPost()
        .jsonBody("{\"title\":\"\",\"tags\":[],\"active\":null,\"sortType\":\"Name\",\"sortAsc\":true,\"searchAfter\":null,\"count\":1000}")
        .responseObject<SearchResult>(json = @UseExperimental(kotlinx.serialization.UnstableDefault::class) Json.nonstrict)

    return when (res) {
        is Result.Failure -> null
        is Result.Success -> res.get()
    }
}
