package net.teppel.helpers

fun String.splitWords() = split("\\P{L}".toRegex())