package net.teppel.helpers

import org.jsoup.nodes.Element

class VolumeComparator : Comparator<Element> {
    private fun String.validRoman(): Boolean =
        matches("^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$".toRegex())

    private fun toArabic(n: String): Int = when {
        n.isBlank() -> 0
        n.startsWith("M") -> 1000 + toArabic(n.removeRange(0, 1))
        n.startsWith("CM") -> 900 + toArabic(n.removeRange(0, 2))
        n.startsWith("D") -> 500 + toArabic(n.removeRange(0, 1))
        n.startsWith("CD") -> 400 + toArabic(n.removeRange(0, 2))
        n.startsWith("C") -> 100 + toArabic(n.removeRange(0, 1))
        n.startsWith("XC") -> 90 + toArabic(n.removeRange(0, 2))
        n.startsWith("L") -> 50 + toArabic(n.removeRange(0, 1))
        n.startsWith("XL") -> 40 + toArabic(n.removeRange(0, 2))
        n.startsWith("X") -> 10 + toArabic(n.removeRange(0, 1))
        n.startsWith("IX") -> 9 + toArabic(n.removeRange(0, 2))
        n.startsWith("V") -> 5 + toArabic(n.removeRange(0, 1))
        n.startsWith("IV") -> 4 + toArabic(n.removeRange(0, 2))
        n.startsWith("I") -> 1 + toArabic(n.removeRange(0, 1))
        else -> throw IllegalArgumentException()
    }

    private fun Element.title() = selectFirst(".title > a").text()
    private fun String.tryInt(): Int = """^.*?(\d+).*$""".toRegex().matchEntire(this)!!.groupValues[0].toInt()

    override fun compare(p0: Element, p1: Element): Int {
        val t1 = p0.title()
        val t2 = p1.title()

        try {
            return compareValues(t1.tryInt(), t2.tryInt())
        } catch (e: RuntimeException) {
            val s1 = t1.splitWords().find { it.validRoman() }
            val s2 = t2.splitWords().find { it.validRoman() }

            if (s1 == null || s2 == null) {
                if (s1 == "INDEX") {
                    return -1
                }
                return 0
            }

            return compareValues(toArabic(s1), toArabic(s2))
        } catch (e: RuntimeException) {
            return 0
        }
    }
}
