package net.teppel.helpers

import org.jsoup.nodes.Element

val chapterTypes = listOf(
    "foreword",
    "preface",
    "prologue",
    "chapter",
    "epilogue",
    "afterword",
    "outro",
    "side story"
)

fun findTitleType(cn: String, matchers: List<String>): String {
    // split into words
    val ts =
        cn.split("[^a-zA-Z]".toRegex()).filter { t -> "[a-zA-Z]".toRegex().containsMatchIn(t) }

    // try to guess the chapter type from it's title
    var tt = chapterTypes.firstOrNull { t: String ->
        ts.fold(false) { acc, s ->
            if (
                t.levenshtein(s.toLowerCase()) <= 1
                || (t === "chapter" && matchers.indexOf(s) != -1)
            ) true
            else acc
        }

    }

    // if we couldn't guess and it's numeric -> chapter, otherwise "other"
    if (tt === null) {
        tt = when (cn.toIntOrNull()) {
            null -> "chapter"
            else -> "other"
        }
    }

    return tt
}

/**
 * Get an alphanumeric string representing the chapter number (counts from 1st chapter mostly)
 */
fun absChapterNumber(path: String, matchers: List<String>): String =
    Regex(
        "(?:.*chapter|${matchers.joinToString("|")})?-?(.*)",
        RegexOption.IGNORE_CASE
    )
        .matchEntire(path.split("/")[3])!!.groupValues[1]

fun Element.elementText(selector: String): String = this.selectFirst(selector).text()
