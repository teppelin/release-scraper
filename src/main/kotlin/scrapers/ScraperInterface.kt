package net.teppel.scrapers

import net.teppel.types.Media
import net.teppel.types.Volume

interface ScraperInterface {
    val baseURL: String
    fun all(): Sequence<Media>
    fun chapterContent(path: String): String
    fun chapters(paths: List<String>, matchers: List<String> = emptyList(), novelURL: String?): List<Volume>
    fun new(): Sequence<Media>
//    fun media(slug: String): Media
}