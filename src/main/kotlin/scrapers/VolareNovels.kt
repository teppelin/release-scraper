package net.teppel.scrapers

import by.heap.remark.Remark
import net.teppel.helpers.absChapterNumber
import net.teppel.helpers.elementText
import net.teppel.helpers.findTitleType
import net.teppel.helpers.novelsList
import net.teppel.types.Chapter
import net.teppel.types.FeedItem
import net.teppel.types.Media
import net.teppel.types.Volume
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.net.URL

private val logger = mu.KotlinLogging.logger {}

class VolareNovels(override val baseURL: String = "https://www.volarenovels.com") : ScraperInterface {
    private val remark: Remark = Remark()

    private fun Document.getTranslator() =
        selectFirst(".p-tb-10-rl-30 > p:nth-child(2)").text().replace("Translated by: ", "")

    override fun chapterContent(path: String): String {
        val view = Jsoup.connect(baseURL + path).get().selectFirst(".fr-view:not(.hidden)")

        // remove bloat from fr-view div
        view.select(".chapter-nav, img, .hidden-text, .__cf_email__, p[data-f-id='pbf'], span[style*=\"font-size: 0\"]")
            .remove()

        return remark.convertFragment(view.html())
    }

    // Return a list of coroutines instead?
    override fun all(): Sequence<Media> = sequence {
        val novels = novelsList("volarenovels")

        novels?.items?.forEach { m ->
            logger.debug { "Parsing '${m.name}'" }

            val novelURL = "$baseURL/novel/${m.slug}"
            val doc = Jsoup.connect(novelURL).get()
            val volumes = mutableListOf<Volume>()
            val translator = doc.getTranslator()

//            doc.select("#accordion > .panel-default").forEachIndexed { i, v ->
            // TODO: .sortedWith(VolumeComparator())
            doc.selectFirst("#accordion").children().forEachIndexed { i, v ->
                val vn = v.selectFirst(".title > a").text()
                val vi = i + 1

                @Suppress("NAME_SHADOWING")
                val chapters = v.select(".chapter-item").mapIndexed { i, c ->
                    val name = c.selectFirst("a span").text()
                    val path = c.selectFirst("a").attr("href")
                    val content = chapterContent(path)
                    val tt = findTitleType(name, listOf(m.abbreviation))

                    Chapter(
                        name,
                        absChapterNumber(path, listOf(m.abbreviation, m.slug)),
                        i + 1,
                        tt,
                        baseURL + path,
                        content,
                        translator
                    )
                }
                volumes.add(Volume(vn, vi, chapters.toMutableList()))
            }
            yield(Media(m.name, m.slug, "volarenovels", volumes.toList()))
        }
    }

    // same here, list of coroutines
    override fun new(): Sequence<Media> = sequence {
        val novels = novelsList("volarenovels")
        val doc = Jsoup.connect("$baseURL/feed/chapters").get()
        doc.select("item").map {
            FeedItem(
                it.elementText("category"),
                it.elementText("title"),
                it.elementText("link")
            )
        }
            .groupBy { it.category }
            .forEach {
                val n = novels?.items?.find { n -> n.name == it.key }
                if (n === null) {
                    return@forEach
                }

                yield(
                    Media(
                        n.name,
                        n.slug,
                        "volarenovels",
                        chapters(
                            it.value.map { x -> URL(x.link).path },
                            listOf(n.abbreviation, n.slug),
                            "$baseURL/novel/${n.slug}"
                        )
                    )
                )
            }
    }

    override fun chapters(paths: List<String>, matchers: List<String>, novelURL: String?): List<Volume> {
        val doc = Jsoup.connect(novelURL).get()
        val volumes = mutableMapOf<Int, Volume>()
        val translator = doc.getTranslator()

        val accordion = doc
            .selectFirst("#accordion")
            .children()
//            .sortedWith(VolumeComparator()) // TODO: lmao

        for (path in paths) {
//            val chapter = accordion.find { it.selectFirst(""".chapter-item > a[href="$path"]""") != null }
            accordion.forEachIndexed { i, el ->
                val chapter = el.selectFirst(""".chapter-item > a[href="$path"]""")

                // return if el null
                chapter ?: return@forEachIndexed

                val c = chapter.parent()
                val cn = c.text()

                val v = c.parents()[5]
                val vi = i + 1
//                val vi = v.elementSiblingIndex() + 1

                // This is for reversed ToC cases.. such as
                // https://www.volarenovels.com/novel/the-sword-and-the-shadow
//                val volName = v.selectFirst("a").text()
//                val volSiblingIndex = v.elementSiblingIndex() + 1
//                val volNameIndex = """^.*?(\d+).*$""".toRegex().matchEntire(volName)?.groupValues?.get(1)?.toInt()
//                val vi = when {
//                    volNameIndex != null && volNameIndex > volSiblingIndex -> volSiblingIndex
//                    else -> volSiblingIndex
//                }

                logger.debug { "${v.selectFirst("a").text()}, $vi" }

                val chpt = Chapter(
                    cn,
                    absChapterNumber(path, matchers),
                    c.parents()[4].select(".chapter-item").indexOf(c) + 1,
                    findTitleType(cn, matchers),
                    baseURL + path,
                    chapterContent(path),
                    translator
                )

                if (volumes.containsKey(vi)) {
                    volumes[vi]!!.Chapters.add(chpt)
                } else {
                    volumes[vi] = Volume(v.selectFirst("a").text(), vi, mutableListOf(chpt))
                }
            }
        }

        return volumes.values.toList()
    }
}
