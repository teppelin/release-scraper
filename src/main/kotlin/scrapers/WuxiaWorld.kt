package net.teppel.scrapers

import by.heap.remark.Remark
import net.teppel.helpers.absChapterNumber
import net.teppel.helpers.elementText
import net.teppel.helpers.findTitleType
import net.teppel.helpers.novelsList
import net.teppel.types.Chapter
import net.teppel.types.FeedItem
import net.teppel.types.Media
import net.teppel.types.Volume
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.net.URL

private val logger = mu.KotlinLogging.logger {}

class WuxiaWorld(override val baseURL: String = "https://www.wuxiaworld.com") : ScraperInterface {
    private val remark: Remark = Remark()

    private fun Document.getTranslator() = selectFirst(".dl-horizontal > dd:nth-child(2)")
        .text()

    // private fun Document.getTitle() = selectFirst(".p-15 > h4:nth-child(1)").text()

    override fun chapterContent(path: String): String {
        val view = Jsoup
            .connect(baseURL + path)
            .get()
            .select(".fr-view")
            .not(".panel-body, .hidden")
            .first()

        // remove bloat from fr-view div
        view.select(".chapter-nav, img, .hidden-text, .__cf_email__, p[data-f-id='pbf']").remove()

        return remark.convertFragment(view.html())
    }

    // Return a list of coroutines instead?
    override fun all(): Sequence<Media> = sequence {
        val novels = novelsList("wuxiaworld")

        novels?.items?.forEach { m ->
            logger.debug { "Parsing '${m.name}'" }

            val novelURL = "$baseURL/novel/${m.slug}"
            val doc = Jsoup.connect(novelURL).get()
            val volumes = mutableListOf<Volume>()
            val translator = doc.getTranslator()

            doc.select("#accordion > .panel-default").forEachIndexed { i, v ->
                val vn = v.select(".title > a").text()
                val vi = i + 1

                @Suppress("NAME_SHADOWING")
                val chapters = v.select(".chapter-item").mapIndexed { i, c ->
                    val name = c.selectFirst("a span").text()
                    val path = c.selectFirst("a").attr("href")
                    val content = chapterContent(path)
                    val tt = findTitleType(name, listOf(m.abbreviation))

                    Chapter(
                        name,
                        absChapterNumber(path, listOf(m.abbreviation, m.slug)),
                        i + 1,
                        tt,
                        baseURL + path,
                        content,
                        translator
                    )
                }
                volumes.add(Volume(vn, vi, chapters.toMutableList()))
            }
            yield(Media(m.name, m.slug, "wuxiaworld", volumes.toList()))
        }
    }

    // same here, list of coroutines
    override fun new(): Sequence<Media> = sequence {
        val novels = novelsList("wuxiaworld")
        val doc = Jsoup.connect("$baseURL/feed/chapters").get()

        doc.select("item").map {
            FeedItem(
                it.elementText("category"),
                it.elementText("title"),
                it.elementText("link")
            )
        }
            .groupBy { it.category }
            .forEach {
                val n = novels?.items?.find { n -> n.name == it.key }
                if (n === null) {
                    return@forEach
                }

                yield(
                    Media(
                        n.name,
                        n.slug,
                        "wuxiaworld",
                        chapters(
                            it.value.map { x -> URL(x.link).path },
                            listOf(n.abbreviation, n.slug),
                            "$baseURL/novel/${n.slug}"
                        )
                    )
                )
            }
    }

    /**
     * @throws NullPointerException - sometimes the release isn't in the novel TOC
     */
    override fun chapters(paths: List<String>, matchers: List<String>, novelURL: String?): List<Volume> {
        val doc = Jsoup.connect(novelURL).get()
        val volumes = mutableMapOf<Int, Volume>()
        val translator = doc.getTranslator()

        for (path in paths) {
            doc.selectFirst(""".chapter-item > a[href="$path"]""")?.let {
                val c = it.parent()

                val v = c.parents()[5]
                val vi = v.elementSiblingIndex() + 1
                val cn = c.text()

                logger.debug { "${v.selectFirst("a").text()}, $vi" }

                val chapter = Chapter(
                    cn,
                    absChapterNumber(path, matchers),
                    c.parents()[4].select(".chapter-item").indexOf(c) + 1,
                    findTitleType(cn, matchers),
                    baseURL + path,
                    chapterContent(path),
                    translator
                )

                if (volumes.containsKey(vi)) {
                    volumes[vi]!!.Chapters.add(chapter)
                } else {
                    volumes[vi] = Volume(v.selectFirst("a").text(), vi, mutableListOf(chapter))
                }
            }
        }

        return volumes.values.toList()
    }
}