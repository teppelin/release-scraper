package net.teppel.scrapers

import by.heap.remark.Remark
import net.teppel.types.Media
import net.teppel.types.Volume
import org.jsoup.Jsoup

private val logger = mu.KotlinLogging.logger {}

class GravityTales(override val baseURL: String = "http://gravitytales.com") : ScraperInterface {
    data class MenuNovel(
        val name: String,
        val url: String
    )

    private val remark: Remark = Remark()

    private fun doc(url: String) = Jsoup
        .connect(url)
        .userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36")
        .get()

    fun list(): List<MenuNovel> =
        doc(baseURL).selectFirst("div.widget:nth-child(6) > ul:nth-child(2)").children().map {
            val a = it.selectFirst("a")
            MenuNovel(a.text(), baseURL + a.attr("href").replace("/posts", ""))
        }

    override fun all(): Sequence<Media> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun chapterContent(path: String): String {
        val content = doc(baseURL + path).selectFirst("#chapterContent")
        return remark.convertFragment(content.html())
    }

    override fun chapters(paths: List<String>, matchers: List<String>, novelURL: String?): List<Volume> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun new(): Sequence<Media> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}