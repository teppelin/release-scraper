package net.teppel.scrapers

class ScraperFactory {
    fun create(source: String): ScraperInterface =
        when (source) {
            "volarenovels" -> VolareNovels()
            "wuxiaworld" -> WuxiaWorld()
            else -> throw NotImplementedError("$source 'source' not implemented.")
        }
}
