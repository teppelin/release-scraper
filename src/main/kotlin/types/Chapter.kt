package net.teppel.types

import kotlinx.serialization.Serializable

@Serializable
data class Chapter(
    var Name: String,
    var AbsoluteNumber: String,
    var Number: Int,
    var Type: String,
    var SourceURL: String,
    var Content: String,
    var Translator: String?,
    var Language: String = "en"
)
