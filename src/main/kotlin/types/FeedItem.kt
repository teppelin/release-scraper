package net.teppel.types

data class FeedItem(
    var category: String,
    var title: String,
    var link: String
)