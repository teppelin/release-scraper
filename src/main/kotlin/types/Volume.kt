package net.teppel.types

import kotlinx.serialization.Serializable

@Serializable
data class Volume(
    var Name: String,
    var Number: Int,
    var Chapters: MutableList<Chapter>
)