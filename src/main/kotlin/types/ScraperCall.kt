package net.teppel.types

import kotlinx.serialization.Serializable

@Serializable
data class Call(
    val method: String,
    val params: CallParams
)

@Serializable
data class CallParams(
    val source: String,
    val first: Int? = null,
    val offset: Int? = null
)
