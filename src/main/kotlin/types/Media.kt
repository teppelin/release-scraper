package net.teppel.types

import kotlinx.serialization.Serializable
import kotlinx.serialization.stringify
import net.teppel.json

@Serializable
data class Media(
    var MediaName: String,
    var MediaSlug: String,
//    var MediaShort: String,
    var ReleaseGroup: String,
    var Volumes: List<Volume>?
) {
    /**
     * probably a bad idea?
     */
    fun toJson(): String =
        @UseExperimental(kotlinx.serialization.ImplicitReflectionSerializer::class) json.stringify(this)
}