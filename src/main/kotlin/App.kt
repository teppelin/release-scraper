package net.teppel

import com.natpryce.konfig.EnvironmentVariables
import com.natpryce.konfig.Key
import com.natpryce.konfig.intType
import com.natpryce.konfig.stringType
import com.rabbitmq.client.AMQP
import com.rabbitmq.client.Channel
import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.Delivery
import com.viartemev.thewhiterabbit.channel.ConfirmChannel
import com.viartemev.thewhiterabbit.channel.consume
import com.viartemev.thewhiterabbit.channel.createConfirmChannel
import com.viartemev.thewhiterabbit.channel.publish
import com.viartemev.thewhiterabbit.publisher.OutboundMessage
import kotlinx.coroutines.*
import kotlinx.io.IOException
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import net.teppel.scrapers.ScraperFactory
import net.teppel.types.Call
import kotlinx.coroutines.channels.Channel as KChannel

private val config = EnvironmentVariables()
private val logger = mu.KotlinLogging.logger {}

val json =
    Json(@UseExperimental(UnstableDefault::class) JsonConfiguration.Default)

private val CONSUMER_QUEUE_NAME = config[Key("queue.consume", stringType)]
private val PUBLISHER_QUEUE_NAME = config[Key("queue.publish", stringType)]
private val PARALLELISM = config[Key("parallelism", intType)]
private val AMQP_URI = config[Key("amqp.uri", stringType)]

fun main() {
    logger.info { "Initializing..." }

    App().run()
}

class App {
    private val factory: ConnectionFactory = ConnectionFactory().apply { useNio() }

    init {
        factory.setUri(AMQP_URI)
    }

    private val connection = factory.newConnection()

    private val consumeChannel: Channel = connection.createChannel()
    private val publishChannel: ConfirmChannel = connection.createConfirmChannel()

    /**
     * Create an outbound AMQP message
     */
    private fun createMessage(body: String) =
        OutboundMessage(
            "",
            PUBLISHER_QUEUE_NAME,
            AMQP.BasicProperties(
                "application/json",
                null,
                null,
                2,
                0,
                null,
                null,
                null,
                null,
                null,
                "releases",
                null,
                null,
                null
            ),
            body
        )

    /**
     * Parse the amqp method call
     */
    private fun parseDelivery(msg: Delivery): Call = json.parse(Call.serializer(), String(msg.body))

    /**
     * Consume the delivery channel
     */
    private suspend fun consume(channel: KChannel<Delivery>) {
        for (msg in channel) {
            try {
                val call = parseDelivery(msg)
                val scraper = ScraperFactory().create(call.params.source)

                // {"method": "all", "params": {"source":"volarenovels"}}
                logger.info { call }

                val mediaSequence = when (call.method) {
                    "all" -> scraper.all()
                    "new" -> scraper.new()
                    else -> throw NotImplementedError("${call.method} 'method' not implemented.")
                }

                for (m in mediaSequence) {
                    logger.info { "Completed '${m.MediaName}' by '${m.ReleaseGroup}' (${m.Volumes?.size} volumes)" }

                    publishChannel.publish {
                        coroutineScope {
                            publishWithConfirmAsync(
                                this.coroutineContext,
                                listOf(createMessage(m.toJson()))
                            ).awaitAll()
                        }
                    }
                }
            } catch (e: NotImplementedError) {
                logger.warn(e) { "Received an unimplemented method call." }
            } catch (e: RuntimeException) {
                logger.error(e) { e }
            } catch (e: IOException) {
                logger.error(e) { e }
            }
        }
    }

    /**
     * Run the app
     */
    fun run() {
        logger.info { "✔️  Connected and listening for jobs." }


        runBlocking {
            val channel = KChannel<Delivery>()
            val handler = CoroutineExceptionHandler { _, exception ->
                logger.error(exception) { "Caught original $exception in main job coroutine." }
            }

            repeat(PARALLELISM) {
                launch(handler) { consume(channel) }
            }

            try {
                consumeChannel.consume(CONSUMER_QUEUE_NAME, 10) {
                    coroutineScope {
                        while (true) {
                            consumeMessageWithConfirm({ channel.send(it) })
                        }
                    }
                }
            } catch (e: RuntimeException) {
                logger.error { e }
            }

        }

        connection.close()
    }
}
